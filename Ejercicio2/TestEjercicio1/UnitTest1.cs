﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ejercicio1;

namespace TestEjercicio1
{
    [TestClass]
    public class calcularMenor3Numeros
    {
        [TestMethod]
        public void cualEsMenor()
        {
            int a = 1;
            int b = 2;
            int c = 3;
            Assert.AreEqual(1, e1Sol.calcularMenor(a, b, c));
        }

        [TestMethod]
        public void cualEsMenorWith0()
        {
            int a = 0;
            int b = 1;
            int c = 2;
            Assert.AreEqual(0, e1Sol.calcularMenor(a, b, c));
        }

        [TestMethod]
        public void cualEsMenorWithNegatives()
        {
            int a = 0;
            int b = 1;
            int c = -2;
            Assert.AreEqual(-2, e1Sol.calcularMenor(a, b, c));
        }

        [TestMethod]
        public void cualEsMenorAllNegatives()
        {
            int a = -344;
            int b = -344;
            int c = -2;
            Assert.AreEqual(-344, e1Sol.calcularMenor(a, b, c));
        }
    }
}
