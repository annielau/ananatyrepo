﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ejercicio3;

namespace UnitTestProject1
{
    [TestClass]
    public class calcularNumExcedeNum
    {
        [TestMethod]
        public void excedeX20()
        {
            int x = 20;
            int num= e3sol.calcularNumero(x);
            Assert.AreEqual(6, num);
        }

        [TestMethod]
        public void excedeX1()
        {
            int x = 1;
            int num = e3sol.calcularNumero(x);
            Assert.AreEqual(2, num);
        }

        [TestMethod]
        public void excedeX2()
        {
            int x = 2;
            int num = e3sol.calcularNumero(x);
            Assert.AreEqual(2, num);
        }

        [TestMethod]
        public void excedeX3()
        {
            int x = 3;
            int num = e3sol.calcularNumero(x);
            Assert.AreEqual(3, num);
        }

        [TestMethod]
        public void excedeX4()
        {
            int x = 4;
            int num = e3sol.calcularNumero(x);
            Assert.AreEqual(3, num);
        }

        [TestMethod]
        public void excedeX10()
        {
            int x = 10;
            int num = e3sol.calcularNumero(x);
            Assert.AreEqual(5, num);
        }
        [TestMethod]
        public void excedeX0()
        {
            int x = 0;
            int num = e3sol.calcularNumero(x);
            Assert.AreEqual(1, num);
        }
        [TestMethod]
        public void excedeX100()
        {
            int x = 100;
            int num = e3sol.calcularNumero(x);
            Assert.AreEqual(14, num);
        }
    }
}

