﻿namespace MailSender
{
    using System.Net.Mail;

    public class GMailSender : IMailSender
    {
        public void SendMail(Mail mail)
        {
            using(var client = this.GetClient())
            {
                client.Send(this.Transform(mail));               
            }            
        }

        private MailMessage Transform(Mail mail)
        {
            var mmsg = new MailMessage();
            mmsg.To.Add(mail.To);
            mmsg.Subject = mail.Subject;
            mmsg.SubjectEncoding = System.Text.Encoding.UTF8;
            mmsg.Body = mail.Body;
            mmsg.BodyEncoding = System.Text.Encoding.UTF8;
            mmsg.IsBodyHtml = false;
            mmsg.From = new MailAddress("sistemareservas.noresponder@gmail.com");
            return mmsg;
        }

        private SmtpClient GetClient()
        {
            var client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential("sistemareservas.noresponder@gmail.com", "trainingrosario");
            client.Port = 587;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            return client;
        }
    }
}
