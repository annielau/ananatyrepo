﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ejercicio4;

namespace TestEjercicio4
{
    [TestClass]
    public class esParImpar
    {
        [TestMethod]
        public void esParTrue()
        {
            int x = 2;
            Assert.IsTrue(e4Sol.calcularParImpar(x)); 

        }

        [TestMethod]
        public void esParFalse()
        {
            int x = 1;
            Assert.IsFalse(e4Sol.calcularParImpar(x));

        }

        [TestMethod]
        public void ParImparCero()
        {
            int x = 0;
            Assert.IsTrue(e4Sol.calcularParImpar(x));

        }
    }
}
